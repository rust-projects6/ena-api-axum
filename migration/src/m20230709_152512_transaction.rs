use sea_orm_migration::prelude::*;

use crate::{m20230709_152251_user::Users, m20230709_152449_vehicle::Vehicle};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Transaction::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Transaction::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Transaction::Type).string().not_null())
                    .col(
                        ColumnDef::new(Transaction::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(ColumnDef::new(Transaction::UpdatedAt).timestamp_with_time_zone())
                    .col(ColumnDef::new(Transaction::UserId).integer().not_null())
                    .col(ColumnDef::new(Transaction::VehicleId).integer().not_null())
                    .to_owned(),
            )
            .await?;

        manager
            .create_foreign_key(
                ForeignKey::create()
                    .name("frk_vehicle")
                    .to(Vehicle::Table, Vehicle::Id)
                    .from(Transaction::Table, Transaction::VehicleId)
                    .to_owned(),
            )
            .await?;
        manager
            .create_foreign_key(
                ForeignKey::create()
                    .name("frk_user")
                    .to(Users::Table, Users::Id)
                    .from(Transaction::Table, Transaction::UserId)
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_foreign_key(ForeignKey::drop().name("frk_vehicle").to_owned())
            .await?;
        manager
            .drop_foreign_key(ForeignKey::drop().name("frk_user").to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Transaction::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum Transaction {
    Table,
    Id,
    Type,
    UserId,
    VehicleId,
    CreatedAt,
    UpdatedAt,
}
