pub use sea_orm_migration::prelude::*;

mod m20230709_152251_user;
mod m20230709_152449_vehicle;
mod m20230709_152512_transaction;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20230709_152251_user::Migration),
            Box::new(m20230709_152449_vehicle::Migration),
            Box::new(m20230709_152512_transaction::Migration),
        ]
    }
}
