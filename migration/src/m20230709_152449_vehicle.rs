use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Vehicle::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Vehicle::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(Vehicle::VehicleNo)
                            .string()
                            .unique_key()
                            .not_null(),
                    )
                    .col(ColumnDef::new(Vehicle::Owner).string().not_null())
                    .col(ColumnDef::new(Vehicle::Fitness).date().not_null())
                    .col(ColumnDef::new(Vehicle::Tax).date().not_null())
                    .col(ColumnDef::new(Vehicle::Route).date().not_null())
                    .col(ColumnDef::new(Vehicle::Insurance).date().not_null())
                    .col(
                        ColumnDef::new(Vehicle::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(Vehicle::UpdatedAt)
                            .timestamp_with_time_zone()
                            .null(),
                    )
                    .col(
                        ColumnDef::new(Vehicle::Sold)
                            .boolean()
                            .default(false)
                            .not_null(),
                    )
                    .to_owned(),
            )
            .await?;

        manager
            .create_index(
                Index::create()
                    .if_not_exists()
                    .name("idx_vehicle_no")
                    .table(Vehicle::Table)
                    .col(Vehicle::VehicleNo)
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_index(Index::drop().name("idx_vehicle_no").to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Vehicle::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
pub enum Vehicle {
    Table,
    Id,
    VehicleNo,
    Owner,
    Route,
    Fitness,
    Tax,
    Insurance,
    CreatedAt,
    UpdatedAt,
    Sold,
}
