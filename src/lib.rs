pub mod entity;
mod routes;
mod types;
mod util;

pub use routes::app;
use std::net::SocketAddr;

pub async fn run() {
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));

    axum::Server::bind(&addr)
        .serve(app(Some(false)).await.into_make_service())
        .await
        .unwrap();
}
