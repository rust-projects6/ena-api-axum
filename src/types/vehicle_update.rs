use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct VehicleUpdate {
    pub vehicle_no: Option<String>,
    pub owner: Option<String>,
    pub tax: Option<String>,
    pub insurance: Option<String>,
    pub fitness: Option<String>,
    pub route: Option<String>,
}
