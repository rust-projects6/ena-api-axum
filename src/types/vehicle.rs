use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Serialize, Deserialize)]
pub struct Vehicle {
    pub vehicle_no: String,
    pub owner: String,
    pub fitness: String,
    pub tax: String,
    pub route: String,
    pub insurance: String,
}
