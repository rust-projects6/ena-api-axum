use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};

use crate::util::hash_pass::HashedPass;

#[derive(Deserialize, Debug, IntoParams, ToSchema)]
pub struct UserRequest {
    /// Username
    pub username: String,
    /// Password of the user
    pub password: String,
}

#[derive(Debug, Serialize, ToSchema)]
pub struct UserResponse {
    pub id: i32,
    pub username: String,
    pub token: String,
}

impl HashedPass for UserRequest {
    fn get_pass(&self) -> &str {
        &self.password
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hashed_pass() {
        let user = UserRequest {
            username: "hiron".to_owned(),
            password: "password".to_owned(),
        };

        let hash = user.hashed_pass();

        assert!(user.verify_pass(&hash));
    }
}
