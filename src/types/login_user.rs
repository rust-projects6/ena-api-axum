use utoipa::ToSchema;

use crate::types::user::UserRequest;
use serde::Deserialize;

#[derive(Deserialize, Debug, ToSchema)]
pub struct LoginUser {
    pub username: String,
    password: String,
    pub remember: Option<bool>,
}

impl LoginUser {
    pub fn get_user(&self) -> UserRequest {
        UserRequest {
            username: self.username.to_owned(),
            password: self.password.to_owned(),
        }
    }
}
