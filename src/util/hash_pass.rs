pub trait HashedPass {
    fn get_pass(&self) -> &str;

    fn hashed_pass(&self) -> String {
        let hash = bcrypt::hash(self.get_pass(), bcrypt::DEFAULT_COST).unwrap();
        hash
    }

    fn verify_pass(&self, hash: &str) -> bool {
        let verify = bcrypt::verify(self.get_pass(), hash).unwrap();
        verify
    }
}
