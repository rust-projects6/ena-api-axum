use dotenvy_macro::dotenv;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};

use super::app_err::AppErr;

use serde::Deserialize;
use serde::Serialize;

#[derive(Deserialize, Serialize)]
pub struct Token {
    iat: usize,
    exp: usize,
}

impl Token {
    pub fn new(exp: usize) -> Self {
        let iat = chrono::Utc::now().timestamp() as usize;
        Self { iat, exp }
    }

    pub fn get_token(&self) -> String {
        let secret = dotenv!("USER_SECRET");
        let token = encode(
            &Header::default(),
            self,
            &EncodingKey::from_secret(secret.as_ref()),
        )
        .unwrap();

        token
    }
}

impl Default for Token {
    fn default() -> Self {
        let now = chrono::Utc::now();
        let iat = now.timestamp() as usize;
        let exp = (now + chrono::Duration::days(30)).timestamp() as usize;

        Self { iat, exp }
    }
}

pub fn is_verify<'a>(token: String) -> Result<bool, AppErr<'a>> {
    let secret = dotenv!("USER_SECRET");

    let _token = decode::<Token>(
        &token,
        &DecodingKey::from_secret(secret.as_ref()),
        &Validation::default(),
    )
    .map_err(|error| match error.kind() {
        jsonwebtoken::errors::ErrorKind::InvalidToken => AppErr::Validation,
        jsonwebtoken::errors::ErrorKind::InvalidSignature => AppErr::Validation,

        jsonwebtoken::errors::ErrorKind::ExpiredSignature => AppErr::Authentication,

        _ => AppErr::Authentication,
    })?;
    //.map_err(|_err| AppErr::Other(StatusCode::FORBIDDEN, "The token is invalid"))?;

    Ok(true)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_token() {
        let token = Token::default();
        assert!(is_verify(token.get_token()).unwrap());
    }
}
