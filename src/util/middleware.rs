use axum::http::header;
use axum::http::Request;
use axum::middleware::Next;
use axum::response::Response;

use axum::extract::State;
use sea_orm::DatabaseConnection;
use std::sync::Arc;

use sea_orm::ColumnTrait;
use sea_orm::EntityTrait;
use sea_orm::QueryFilter;

use crate::entity::users;
use crate::util::token;

use super::app_err::AppErr;

pub async fn auth<'a, B>(
    State(db): State<Arc<DatabaseConnection>>,
    mut req: Request<B>,
    next: Next<B>,
) -> Result<Response, AppErr<'a>> {
    let header_token = req
        .headers()
        .get(header::AUTHORIZATION)
        .and_then(|v| v.to_str().ok())
        .ok_or(AppErr::BadRequest)?
        .replace("Bearer ", "");

    let user = users::Entity::find()
        .filter(users::Column::Token.eq(header_token))
        .one(&db as &DatabaseConnection)
        .await
        .map_err(|_| AppErr::DbConnection)?
        .ok_or(AppErr::Authentication)?;

    let valid = token::is_verify(user.token.clone().unwrap())?;

    match valid {
        true => {
            req.extensions_mut().insert(user);
            Ok(next.run(req).await)
        }
        false => Err(AppErr::Authentication),
    }
}
