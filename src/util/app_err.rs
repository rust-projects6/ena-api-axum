use axum::{
    //body::{self, Bytes},
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};

use serde::Serialize;
use utoipa::ToSchema;

#[derive(Debug)]
pub enum AppErr<'a> {
    DbConnection,
    NotFound,
    Authentication,
    BadRequest,
    Validation,
    Other(StatusCode, &'a str),
}

impl<'a> IntoResponse for AppErr<'a> {
    fn into_response(self) -> Response {
        let res = match self {
            AppErr::DbConnection => (
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(ErrMsg {
                    message: "Database is not connected",
                }),
            ),
            AppErr::NotFound => (
                StatusCode::NOT_FOUND,
                Json(ErrMsg {
                    message: "Record is not Found",
                }),
            ),
            AppErr::Authentication => (
                StatusCode::UNAUTHORIZED,
                Json(ErrMsg {
                    message: "Authentication Fail",
                }),
            ),
            AppErr::BadRequest => (
                StatusCode::BAD_REQUEST,
                Json(ErrMsg {
                    message: "Some required fields are missing",
                }),
            ),
            AppErr::Other(err, msg) => (err, Json(ErrMsg { message: msg })),
            AppErr::Validation => (
                StatusCode::FORBIDDEN,
                Json(ErrMsg {
                    message: "This is a invalid requesd",
                }),
            ),
            _ => (
                StatusCode::SERVICE_UNAVAILABLE,
                Json(ErrMsg {
                    message: "Unknown Error!!",
                }),
            ),
        };

        res.into_response()
    }
}

#[derive(Debug, Serialize, ToSchema)]
pub struct ErrMsg<'a> {
    message: &'a str,
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//
//     #[tokio::test]
//     async fn test_AppErr() {
//         let err = AppErr::DbConnection.into_response();
//
//         debug_assert_eq!(
//             err,
//             (
//                 StatusCode::INTERNAL_SERVER_ERROR,
//                 Json(ErrMsg {
//                     message: "Database is not connected",
//                 }),
//             )
//                 .into_response()
//         );
//     }
// }
