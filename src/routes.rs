mod hello;
mod login;
mod signout;
mod signup;
mod vehicle;

use crate::routes::vehicle::get_all_vehicles::get_all_vehicles;
use crate::routes::vehicle::get_fitness::get_fitness;
use crate::routes::vehicle::get_insurance::get_insurance;
use crate::routes::vehicle::get_route::get_route;
use crate::routes::vehicle::get_tax::get_tax;
use crate::routes::vehicle::update_vehicle::update_vehicle;
use crate::util::middleware::auth;
use crate::{
    types::{login_user, user},
    util::app_err,
};
use axum::routing::{get, patch, post};
use axum::Router;
use axum::{http::StatusCode, middleware};
use dotenvy_macro::dotenv;
use hello::hello_world;
use login::login;
use sea_orm::Database;
use signout::sign_out;
use signup::sign_up;
use std::sync::Arc;
use utoipa::openapi::security::{HttpBuilder, SecurityScheme};
use utoipa::Modify;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use vehicle::add_vehicle::add_vehicle;
use vehicle::find_vehicle::find_vehicle;

#[derive(OpenApi)]
#[openapi(
    paths(hello::hello_world, signup::sign_up, login::login, ),
    components(schemas(user::UserResponse, app_err::ErrMsg, user::UserRequest, login_user::LoginUser)),
    modifiers(&SecurityAddons),
    tags((name="ena_api", description="Api for Ena Trasport"))
  )]
struct ApiDoc;

struct SecurityAddons;

impl Modify for SecurityAddons {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        openapi.components = Some(
            utoipa::openapi::ComponentsBuilder::new()
                // .middleware(auth)
                .security_scheme(
                    "api_jwt_token",
                    SecurityScheme::Http(
                        HttpBuilder::new()
                            .scheme(utoipa::openapi::security::HttpAuthScheme::Bearer)
                            .bearer_format("JWT")
                            .build(),
                    ),
                )
                .build(),
        )
    }
}

pub async fn app(test: Option<bool>) -> Router {
    let uri = get_db_url(test.unwrap());
    dbg!(uri);

    let database = Database::connect(uri)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
        .unwrap();
    let state = Arc::new(database);

    dbg!(&state);

    Router::new()
        .merge(SwaggerUi::new("/swagger-ui").url("/api-docs/openapi.json", ApiDoc::openapi()))
        .route("/signout", get(sign_out))
        .route("/vehicle", post(add_vehicle))
        .route("/vehicle/:id", get(find_vehicle))
        .route("/vehicle", get(get_all_vehicles))
        .route("/fitness/:day", get(get_fitness))
        .route("/tax/:day", get(get_tax))
        .route("/insurance/:day", get(get_insurance))
        .route("/route/:day", get(get_route))
        .route("/vehicle/:id", patch(update_vehicle))
        .route_layer(middleware::from_fn_with_state(state.clone(), auth))
        .route("/", get(hello_world))
        .route("/signup", post(sign_up))
        .route("/login", post(login))
        .with_state(state)
}

fn get_db_url(test: bool) -> &'static str {
    if test {
        dotenv!("DATABASE_TEST_URL")
    } else {
        dotenv!("DATABASE_URL")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_db_url() {
        let url = get_db_url(true);
        assert_eq!(url, dotenv!("DATABASE_TEST_URL"));
        let url = get_db_url(false);
        assert_eq!(url, dotenv!("DATABASE_URL"));
    }
}
