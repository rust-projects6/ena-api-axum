use axum::http::StatusCode;

// use hyper::StatusCode;

#[utoipa::path(
    get, 
    path = "/",
    responses(
        (status=200, description = "hello msg form server", body = String )
    )
)]
pub async fn hello_world()->Result<String, StatusCode>{

    // Err(StatusCode::Ok(result))
    
    Ok("Hello World!!".to_owned())
}

#[cfg(test)]
mod tests{
    use super::*;

    #[tokio::test]
    async fn test_hello_world(){
        let result = hello_world().await;
        assert_eq!(result.unwrap(), "Hello World!!");
    }
}
