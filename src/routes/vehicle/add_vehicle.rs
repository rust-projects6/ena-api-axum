use std::sync::Arc;

use axum::{
    extract::State,
    http::StatusCode,
    response::{IntoResponse, Response},
};
use sea_orm::{ActiveModelTrait, DatabaseConnection};

use crate::{entity::vehicle, types::vehicle::Vehicle, util::app_err::AppErr};
use axum::Json;
use chrono::NaiveDate;
use sea_orm::ActiveValue::Set;

pub async fn add_vehicle<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Json(vehicle): Json<Vehicle>,
) -> Result<Response, AppErr<'a>> {
    let db: &DatabaseConnection = &db;

    vehicle::ActiveModel {
        vehicle_no: sea_orm::ActiveValue::Set(vehicle.vehicle_no),
        owner: sea_orm::ActiveValue::Set(vehicle.owner),
        fitness: Set(NaiveDate::parse_from_str(&vehicle.fitness, "%Y-%m-%d")
            .map_err(|_| AppErr::Validation)?),
        route: Set(NaiveDate::parse_from_str(&vehicle.route, "%Y-%m-%d")
            .map_err(|_| AppErr::Validation)?),
        tax: Set(
            NaiveDate::parse_from_str(&vehicle.tax, "%Y-%m-%d").map_err(|_| AppErr::Validation)?
        ),
        insurance: Set(NaiveDate::parse_from_str(&vehicle.insurance, "%Y-%m-%d")
            .map_err(|_| AppErr::Validation)?),
        created_at: Set(chrono::Utc::now().into()),
        ..Default::default()
    }
    .save(db)
    .await
    .map_err(|err| match err {
        _ => {
            // dbg!(&err);
            let msg = err
                .to_string()
                .split(":")
                .take(3)
                .last()
                .unwrap_or("Databse is not connected")
                .to_owned();
            dbg!(&msg);

            let msg: &'static str = Box::leak(msg.into_boxed_str());

            // AppErr::DbConnection
            AppErr::Other(StatusCode::INTERNAL_SERVER_ERROR, msg.trim())
        }
    })?;

    Ok((StatusCode::CREATED, "Vehicle is added").into_response())
}

#[cfg(test)]
mod tests {
    use sea_orm::{MockDatabase, MockExecResult};

    use super::*;

    #[tokio::test]
    async fn test_add_vehicle() {
        let db = MockDatabase::new(sea_orm::DatabaseBackend::Postgres)
            .append_query_results([vec![vehicle::Model {
                id: 23,
                vehicle_no: "D-M-A-234-90".to_owned(),
                owner: "Ena Enterprise".to_owned(),
                fitness: NaiveDate::parse_from_str("2023-07-22", "%Y-%m-%d").unwrap(),
                tax: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                route: NaiveDate::parse_from_str("2023-07-25", "%Y-%m-%d").unwrap(),
                insurance: NaiveDate::parse_from_str("2023-07-27", "%Y-%m-%d").unwrap(),
                created_at: chrono::Utc::now().into(),
                updated_at: None,
                sold: false,
            }]])
            .append_exec_results([MockExecResult {
                last_insert_id: 23,
                rows_affected: 1,
            }])
            .into_connection();

        let req = Vehicle {
            vehicle_no: "adbd".to_string(),
            owner: "hiron".to_string(),
            fitness: "2023-04-08".to_string(),
            tax: "2023-04-08".to_string(),
            route: "2023-04-08".to_string(),
            insurance: "2023-04-08".to_string(),
        };

        let res = add_vehicle(State(Arc::new(db)), Json(req)).await.unwrap();

        assert_eq!(res.status(), StatusCode::CREATED);
    }
}
