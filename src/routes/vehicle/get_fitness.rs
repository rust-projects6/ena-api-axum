use std::sync::Arc;

use axum::http::StatusCode;
use axum::{
    extract::{Path, State},
    response::{IntoResponse, Response},
    Json,
};
use chrono::Duration;
use sea_orm::{ColumnTrait, Condition, DatabaseConnection, EntityTrait, QueryFilter};

use crate::{entity::vehicle, types::vehicle::Vehicle, util::app_err::AppErr};

pub async fn get_fitness<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Path(day): Path<i64>,
) -> Result<Response, AppErr<'a>> {
    let db: &DatabaseConnection = &db;
    let untill_date = (chrono::Utc::now() + Duration::days(day)).date_naive();
    dbg!(&untill_date);

    let vehicles: Vec<Vehicle> = vehicle::Entity::find()
        .filter(
            Condition::all()
                .add(vehicle::Column::Fitness.gte(chrono::Utc::now().date_naive()))
                .add(vehicle::Column::Fitness.lte(untill_date)),
        )
        .all(db)
        .await
        .map_err(|_| AppErr::DbConnection)?
        .into_iter()
        .map(|d| Vehicle {
            vehicle_no: d.vehicle_no,
            owner: d.owner,
            fitness: d.fitness.to_string(),
            tax: d.tax.to_string(),
            insurance: d.insurance.to_string(),
            route: d.route.to_string(),
        })
        .collect();

    Ok((StatusCode::OK, Json(vehicles)).into_response())
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use hyper::body::to_bytes;
    use sea_orm::MockDatabase;

    use super::*;

    #[tokio::test]
    async fn test_get_fitness() {
        let db = MockDatabase::new(sea_orm::DatabaseBackend::Postgres)
            .append_query_results([vec![
                vehicle::Model {
                    id: 23,
                    vehicle_no: "D-M-A-234-90".to_owned(),
                    owner: "Ena Enterprise".to_owned(),
                    fitness: NaiveDate::parse_from_str("2023-07-22", "%Y-%m-%d").unwrap(),
                    tax: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                    route: NaiveDate::parse_from_str("2023-07-25", "%Y-%m-%d").unwrap(),
                    insurance: NaiveDate::parse_from_str("2023-07-27", "%Y-%m-%d").unwrap(),
                    created_at: chrono::Utc::now().into(),
                    updated_at: None,
                    sold: false,
                },
                vehicle::Model {
                    id: 24,
                    vehicle_no: "D-M-kA-34-910".to_owned(),
                    owner: "Ena Enterprise".to_owned(),
                    fitness: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                    tax: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                    route: NaiveDate::parse_from_str("2023-07-25", "%Y-%m-%d").unwrap(),
                    insurance: NaiveDate::parse_from_str("2023-07-27", "%Y-%m-%d").unwrap(),
                    created_at: chrono::Utc::now().into(),
                    updated_at: None,
                    sold: false,
                },
            ]])
            .into_connection();

        let res = get_fitness(State(Arc::new(db)), Path(12)).await.unwrap();
        assert_eq!(res.status(), StatusCode::OK);

        let body = to_bytes(res.into_body()).await.unwrap();

        let body = String::from_utf8(body.to_vec()).unwrap();

        let body: Vec<Vehicle> = serde_json::from_str(&body).unwrap();
        dbg!(&body);

        assert_eq!(body.len(), 2);
    }
}
