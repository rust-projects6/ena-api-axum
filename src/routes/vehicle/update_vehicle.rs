use std::sync::Arc;

use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::{
    extract::{Path, State},
    response::Response,
    Json,
};
use chrono::NaiveDate;
use sea_orm::{ActiveModelTrait, DatabaseConnection, EntityTrait, IntoActiveModel, Set};

use crate::{
    entity::vehicle,
    types::{vehicle_update::VehicleUpdate, FORMAT},
    util::app_err::AppErr,
};

pub async fn update_vehicle<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Path(id): Path<i32>,
    Json(body): Json<VehicleUpdate>,
) -> Result<Response, AppErr<'a>> {
    let db: &DatabaseConnection = &db;

    let mut vehicle = vehicle::Entity::find_by_id(id)
        .one(db)
        .await
        .map_err(|_| AppErr::DbConnection)?
        .ok_or_else(|| AppErr::NotFound)?
        .into_active_model();

    if let Some(vehicle_no) = body.vehicle_no {
        vehicle.vehicle_no = sea_orm::Set(vehicle_no);
        vehicle.updated_at = Set(Some(chrono::Utc::now().into()));
    }

    if let Some(owner) = body.owner {
        vehicle.owner = Set(owner);
        vehicle.updated_at = Set(Some(chrono::Utc::now().into()));
    }

    if let Some(fitness) = body.fitness {
        vehicle.fitness =
            Set(NaiveDate::parse_from_str(&fitness, FORMAT).map_err(|_| AppErr::Validation)?);
        vehicle.updated_at = Set(Some(chrono::Utc::now().into()));
    }

    if let Some(tax) = body.tax {
        vehicle.tax = Set(NaiveDate::parse_from_str(&tax, FORMAT).map_err(|_| AppErr::Validation)?);
        vehicle.updated_at = Set(Some(chrono::Utc::now().into()));
    }
    if let Some(route) = body.route {
        vehicle.route =
            Set(NaiveDate::parse_from_str(&route, FORMAT).map_err(|_| AppErr::Validation)?);
        vehicle.updated_at = Set(Some(chrono::Utc::now().into()));
    }
    if let Some(insurance) = body.insurance {
        vehicle.insurance =
            Set(NaiveDate::parse_from_str(&insurance, FORMAT).map_err(|_| AppErr::Validation)?);
        vehicle.updated_at = Set(Some(chrono::Utc::now().into()));
    }

    vehicle.update(db).await.map_err(|err| match err {
        _ => {
            // dbg!(&err);
            let msg = err
                .to_string()
                .split(":")
                .take(3)
                .last()
                .unwrap_or("Databse is not connected")
                .to_owned();
            dbg!(&msg);

            let msg: &'static str = Box::leak(msg.into_boxed_str());

            // AppErr::DbConnection
            AppErr::Other(StatusCode::INTERNAL_SERVER_ERROR, msg.trim())
        }
    })?;

    Ok((
        StatusCode::OK,
        r#"{"message":"vehicle updates successfully."}"#,
    )
        .into_response())
}
