use std::sync::Arc;

use axum::{
    extract::{Query, State},
    Json,
};
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter};
use serde::Deserialize;

use crate::{entity::vehicle, types::vehicle::Vehicle, util::app_err::AppErr};

#[derive(Deserialize, Debug)]
pub struct SearchQuery {
    search: Option<String>,
}

pub async fn get_all_vehicles<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Query(query): Query<SearchQuery>,
) -> Result<Json<Vec<Vehicle>>, AppErr<'a>> {
    let db: &DatabaseConnection = &db;

    // dbg!(query.search);
    //

    let vehicles: Vec<Vehicle> = if let Some(search) = query.search {
        vehicle::Entity::find()
            .filter(vehicle::Column::VehicleNo.contains(search.trim()))
            .all(db)
            .await
            .map_err(|_| AppErr::DbConnection)?
            .into_iter()
            .map(|v| Vehicle {
                vehicle_no: v.vehicle_no,
                owner: v.owner,
                fitness: v.fitness.to_string(),
                tax: v.tax.to_string(),
                insurance: v.insurance.to_string(),
                route: v.route.to_string(),
            })
            .collect()
    } else {
        vehicle::Entity::find()
            .all(db)
            .await
            .map_err(|_| AppErr::DbConnection)?
            .into_iter()
            .map(|v| Vehicle {
                vehicle_no: v.vehicle_no,
                owner: v.owner,
                fitness: v.fitness.to_string(),
                tax: v.tax.to_string(),
                insurance: v.insurance.to_string(),
                route: v.route.to_string(),
            })
            .collect()
    };

    Ok(Json(vehicles))
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use sea_orm::MockDatabase;

    use super::*;

    #[tokio::test]
    async fn test_get_all_vehicle() {
        let db = MockDatabase::new(sea_orm::DatabaseBackend::Postgres)
            .append_query_results([vec![
                vehicle::Model {
                    id: 23,
                    vehicle_no: "D-M-A-234-90".to_owned(),
                    owner: "Ena Enterprise".to_owned(),
                    fitness: NaiveDate::parse_from_str("2023-07-22", "%Y-%m-%d").unwrap(),
                    tax: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                    route: NaiveDate::parse_from_str("2023-07-25", "%Y-%m-%d").unwrap(),
                    insurance: NaiveDate::parse_from_str("2023-07-27", "%Y-%m-%d").unwrap(),
                    created_at: chrono::Utc::now().into(),
                    updated_at: None,
                    sold: false,
                },
                vehicle::Model {
                    id: 24,
                    vehicle_no: "D-M-kA-34-910".to_owned(),
                    owner: "Ena Enterprise".to_owned(),
                    fitness: NaiveDate::parse_from_str("2023-07-22", "%Y-%m-%d").unwrap(),
                    tax: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                    route: NaiveDate::parse_from_str("2023-07-25", "%Y-%m-%d").unwrap(),
                    insurance: NaiveDate::parse_from_str("2023-07-27", "%Y-%m-%d").unwrap(),
                    created_at: chrono::Utc::now().into(),
                    updated_at: None,
                    sold: false,
                },
            ]])
            .into_connection();

        let Json(res) = get_all_vehicles(State(Arc::new(db)), Query(SearchQuery { search: None }))
            .await
            .unwrap();

        assert_eq!(res.len(), 2);
    }
}
