use std::sync::Arc;

use axum::extract::Path;
use axum::Json;
use axum::{extract::State, response::Response};
use sea_orm::{DatabaseConnection, EntityTrait};

use crate::entity::vehicle;
use crate::types::vehicle::Vehicle;
use crate::util::app_err::AppErr;
use axum::http::StatusCode;
use axum::response::IntoResponse;

pub async fn find_vehicle<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Path(id): Path<i32>,
) -> Result<Response, AppErr<'a>> {
    let db: &DatabaseConnection = &db;

    let vehicle = vehicle::Entity::find_by_id(id)
        .one(db)
        .await
        .map_err(|err| {
            dbg!(&err);
            match err {
                //sea_orm::DbErr::Query(_) => AppErr::BadRequest,
                // sea_orm::DbErr::RecordNotFound(msg) => {
                //     dbg!(msg);
                //     AppErr::NotFound
                // }
                _ => AppErr::DbConnection,
            }
        })?
        .and_then(|value| {
            Some(Vehicle {
                vehicle_no: value.vehicle_no,
                owner: value.owner,
                tax: value.tax.to_string(),
                fitness: value.fitness.to_string(),
                route: value.route.to_string(),
                insurance: value.insurance.to_string(),
            })
        })
        .ok_or(AppErr::NotFound)?;

    Ok((StatusCode::OK, Json(vehicle)).into_response())
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;
    use sea_orm::MockDatabase;

    use super::*;

    #[tokio::test]
    async fn test_find_vehicle() {
        let db = MockDatabase::new(sea_orm::DatabaseBackend::Postgres)
            .append_query_results([vec![vehicle::Model {
                id: 23,
                vehicle_no: "D-M-A-234-90".to_owned(),
                owner: "Ena Enterprise".to_owned(),
                fitness: NaiveDate::parse_from_str("2023-07-22", "%Y-%m-%d").unwrap(),
                tax: NaiveDate::parse_from_str("2023-07-30", "%Y-%m-%d").unwrap(),
                route: NaiveDate::parse_from_str("2023-07-25", "%Y-%m-%d").unwrap(),
                insurance: NaiveDate::parse_from_str("2023-07-27", "%Y-%m-%d").unwrap(),
                created_at: chrono::Utc::now().into(),
                updated_at: None,
                sold: false,
            }]])
            .into_connection();

        let db = Arc::new(db);

        let res = find_vehicle(State(db.clone()), Path(13)).await.unwrap();
        let res1 = find_vehicle(State(db), Path(20))
            .await
            .expect_err("Database Error");

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(
            res1.into_response().status(),
            StatusCode::INTERNAL_SERVER_ERROR
        );
    }
}
