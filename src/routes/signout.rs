use std::sync::Arc;

use axum::{
    extract::State,
    response::{IntoResponse, Response},
    Extension,
};

use axum::http::StatusCode;
use sea_orm::{ActiveModelTrait, ActiveValue::Set, DatabaseConnection, IntoActiveModel};

use crate::{entity::users::Model, util::app_err::AppErr};

///User Logout
///
///logout successfully
#[utoipa::path(
    get,
    path = "/signout",
    responses(
      (status = 200, description = "Success message", body = String),
      (status = 401, description = "Unauthorize", body =  ErrMsg)
    ),
    security((),("Authorization"=[]))
)]
pub async fn sign_out<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Extension(user): Extension<Model>,
) -> Result<Response, AppErr<'a>> {
    let _user = {
        let mut user = user.into_active_model();
        user.token = Set(None);
        user.updated_at = Set(Some(chrono::Utc::now().into()));
        user
    }
    .update(&db as &DatabaseConnection)
    .await
    .map_err(|_| AppErr::DbConnection)?;

    let msg: &str = r#"{"message":"Logout successful!!"}"#;

    Ok((StatusCode::OK, msg).into_response())
}

#[cfg(test)]
mod tests {

    use hyper::body::to_bytes;
    use sea_orm::{DatabaseBackend, MockDatabase};

    use crate::entity::users;

    use super::*;

    #[tokio::test]
    async fn test_signout() {
        let db = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![users::Model {
                id: 1,
                username: "hiron".to_owned(),
                password: "password".to_owned(),
                token: Some("aserxiogejeotted".to_owned()),
                created_at: chrono::Utc::now().into(),
                updated_at: None,
            }]])
            .into_connection();

        let res = sign_out(
            State(Arc::new(db)),
            Extension(users::Model {
                id: 1,
                username: "hiron".to_owned(),
                password: "password".to_owned(),
                token: Some("aserxiogejeotted".to_owned()),
                created_at: chrono::Utc::now().into(),
                updated_at: None,
            }),
        )
        .await
        .unwrap();

        assert_eq!(res.status(), StatusCode::OK);

        let msg = to_bytes(res.into_body()).await.unwrap();

        assert_eq!(&msg, r#"{"message":"Logout successful!!"}"#);
    }
}
