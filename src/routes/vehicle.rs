pub mod add_vehicle;
pub mod find_vehicle;
pub mod get_all_vehicles;
pub mod get_fitness;
pub mod get_insurance;
pub mod get_route;
pub mod get_tax;
pub mod update_vehicle;

//COPY vehicle (vehicle_no, owner, insurance, fitness, tax, route) from 'C:\Users\LENOVO\Documents\vehicle_new.csv' delimiter ',' csv header;
