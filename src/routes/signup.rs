use axum::response::{IntoResponse, Response};
use axum::Json;
use sea_orm::{ActiveModelTrait, ActiveValue::Set};
use sea_orm::{DatabaseConnection, DbErr};
use std::sync::Arc;

use crate::types::user::UserResponse;
use crate::util::token::Token;
use crate::{
    entity::users,
    types::user::UserRequest,
    util::{app_err::AppErr, hash_pass::HashedPass},
};
use axum::extract::State;
use axum::http::StatusCode;

/// Create new User
///
/// Get the user details with Token
#[utoipa::path(
    post,
    path = "/signup",
    request_body = UserRequest,

    responses(
    (status=201, description = "user id with token msg", body = UserResponse),
    (status= 500, description = "Server Errors", body = ErrMsg)
    ),
        // params(
    //     UserRequest
    // )
)]
pub async fn sign_up<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Json(user): Json<UserRequest>,
) -> Result<Response, AppErr<'a>> {
    let db: &DatabaseConnection = &db;

    dbg!(&db);

    let token = Token::default().get_token();

    let new_user = users::ActiveModel {
        username: Set(user.username.to_owned()),
        password: Set(user.hashed_pass().to_owned()),
        token: Set(Some(token)),
        created_at: Set(chrono::Utc::now().into()),
        ..Default::default()
    }
    .save(db)
    .await
    .map_err(|err| match err {
        DbErr::RecordNotInserted => AppErr::Other(StatusCode::BAD_REQUEST, "User already exsist"),
        DbErr::RecordNotUpdated => AppErr::Other(StatusCode::BAD_REQUEST, "User already exsist"),

        _ => {
            // dbg!(&err);
            let msg = err
                .to_string()
                .split(":")
                .take(3)
                .last()
                .unwrap_or("Databse is not connected")
                .to_owned();
            dbg!(&msg);

            let msg: &'static str = Box::leak(msg.into_boxed_str());

            // AppErr::DbConnection
            AppErr::Other(StatusCode::INTERNAL_SERVER_ERROR, msg.trim())
        }
    })?;

    Ok((
        StatusCode::CREATED,
        Json(UserResponse {
            id: new_user.id.unwrap(),
            username: new_user.username.unwrap(),
            token: new_user.token.unwrap().unwrap(),
        }),
    )
        .into_response())
}

#[cfg(test)]
mod tests {

    // use axum::response::IntoResponse;
    use sea_orm::{DatabaseBackend, MockDatabase};

    use super::*;

    #[tokio::test]
    async fn test_signup() -> Result<(), AppErr<'static>> {
        let db = MockDatabase::new(DatabaseBackend::Postgres)
            .append_query_results([vec![users::Model {
                id: 1,
                username: "hiron1".to_owned(),
                password: "sdferwter".to_owned(),
                token: Some("sdfsfeer".to_owned()),
                created_at: chrono::Utc::now().into(),
                updated_at: None,
            }]])
            .into_connection();

        let user = UserRequest {
            username: "hiron1".to_owned(),
            password: "password".to_owned(),
        };

        let res = sign_up(State(Arc::new(db)), Json(user)).await?;

        // cfg!(res);

        // assert_eq!(res.username, "hiron1");
        // assert_eq!(res.id, 1);
        assert_eq!(res.status(), 201);
        Ok(())
    }
}
