use crate::util::hash_pass::HashedPass;
use axum::{extract::State, Json};
use sea_orm::ColumnTrait;
use sea_orm::{
    ActiveModelTrait, ActiveValue::Set, DatabaseConnection, DbErr, EntityTrait, IntoActiveModel,
    QueryFilter,
};
use std::sync::Arc;

use crate::{
    entity::users,
    types::login_user::LoginUser,
    types::user::UserResponse,
    util::{app_err::AppErr, token::Token},
};

/// User Login
///
/// Get the user details with Token
#[utoipa::path(
    post,
    path = "/login",
    request_body = LoginUser,
    responses(
        (status=200, description = "user id with token msg", body = UserResponse),
        (status= 500, description = "Server Errors", body = ErrMsg),
        (status= 401, description = "UnAuthorized", body = ErrMsg)
    ),
        // params(
    //     UserRequest
    // )
)]
pub async fn login<'a>(
    State(db): State<Arc<DatabaseConnection>>,
    Json(req): Json<LoginUser>,
) -> Result<Json<UserResponse>, AppErr<'a>> {
    let db: &DatabaseConnection = &db;

    let user = users::Entity::find()
        .filter(users::Column::Username.eq(&req.username))
        .one(db)
        .await
        .map_err(|err| match err {
            DbErr::RecordNotFound(_) => AppErr::NotFound,
            _ => AppErr::DbConnection,
        })?
        .ok_or(AppErr::NotFound)?;

    let verify = req.get_user().verify_pass(&user.password);

    if !verify {
        return Err(AppErr::Authentication);
    }

    let token = req
        .remember
        .and_then(|value| match value {
            true => {
                let exp = (chrono::Utc::now() + chrono::Duration::days(365)).timestamp() as usize;
                Some(Token::new(exp).get_token())
            }
            false => Some(Token::default().get_token()),
        })
        .or_else(|| Some(Token::default().get_token()))
        .unwrap();

    let mut user = user.into_active_model();

    user.token = sea_orm::ActiveValue::Set(Some(token));
    user.updated_at = Set(Some(chrono::Utc::now().into()));

    let user = user.save(db).await.map_err(|_| AppErr::DbConnection)?;

    let res = UserResponse {
        id: user.id.unwrap(),
        username: user.username.unwrap().to_owned(),
        token: user.token.unwrap().unwrap(),
    };

    Ok(Json(res))
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     use sea_orm::DatabaseBackend;
//     use sea_orm::MockDatabase;
//
//     #[tokio::test]
//     async fn test_login() {
//         let db = MockDatabase::new(DatabaseBackend::Postgres)
//             .append_query_results([vec![users::Model {
//                 id: 1,
//                 username: "hiron1".to_owned(),
//                 password: "sdferwter".to_owned(),
//                 token: Some("sdfsfeer".to_owned()),
//                 created_at: chrono::Utc::now().into(),
//                 updated_at: None,
//             }]])
//             .into_connection();
//
//         let user = UserRequest {
//             username: "hiron1".to_owned(),
//             password: "sdferwter".to_owned(),
//         };
//
//         let Json(res) = login(State(Arc::new(db)), Json(user)).await.unwrap();
//
//         assert_eq!(res.token, "sdfsfeer");
//     }
// }
