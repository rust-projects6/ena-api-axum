pub mod login_user;
pub mod user;
pub mod vehicle;
pub mod vehicle_update;

pub const FORMAT: &str = "%Y-%m-%d";
