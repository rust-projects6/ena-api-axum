use dotenvy::dotenv;
use ena_api_axum::run;

#[tokio::main]
async fn main() {
    // println!("Hello, world!");
    dotenv().ok();
    run().await;
}
