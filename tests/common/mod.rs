// use dotenvy::dotenv;

use axum::body::Body;
use axum::http::Request;
use axum::Router;

use dotenvy_macro::dotenv;
// use ena_api_axum::app;
use ena_api_axum::entity::prelude::{Transaction, Users, Vehicle};
use hyper::body::to_bytes;
use serde_json::Value;
use tower::ServiceExt;

use sea_orm::{Database, DatabaseConnection, EntityTrait};

pub enum Table {
    USERS,
    VEHICLE,
    TRANSACTION,
}

// #[async_trait]
pub async fn empty_tables(table: Table) -> Result<(), String> {
    // dotenv().ok();
    let uri = dotenv!("DATABASE_TEST_URL");
    let db: DatabaseConnection = Database::connect(uri).await.unwrap();

    match table {
        Table::USERS => {
            Users::delete_many()
                .exec(&db)
                .await
                .map_err(|_| "Users Table is not droped".to_owned())?;
            Ok(())
        }
        Table::VEHICLE => {
            Vehicle::delete_many()
                .exec(&db)
                .await
                .map_err(|_| "Vehicle Table is not droped".to_owned())?;
            Ok(())
        }
        Table::TRANSACTION => {
            Transaction::delete_many()
                .exec(&db)
                .await
                .map_err(|_| "Transaction Table is not droped".to_owned())?;
            Ok(())
        }
    }
}

// async fn drop_table(db: DatabaseConnection, entity: EntityTrait) -> Result<(), String> {
//     entity::delete_many()
//         .exec(&db)
//         .await
//         .map_err(|_| "Table{entity} is not dorpes".to_owned())?;
//
//     Ok(())
// }
//

pub async fn get_token(app: Router) -> Result<String, String> {
    let uri = dotenv!("DATABASE_TEST_URL");
    let db: DatabaseConnection = Database::connect(uri).await.unwrap();
    //let app = app(Some(true)).await;

    let all_user = Users::find().all(&db).await.unwrap();

    let req = r#"
                 {
                    "username":"hiron",
                    "password":"password"
                 }
                 "#;

    dbg!(&all_user.is_empty());

    let response = if let true = all_user.is_empty() {
        app.oneshot(
            Request::builder()
                .uri("/signup")
                .method(axum::http::Method::POST)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .body(Body::from(req))
                .unwrap(),
        )
        .await
        .unwrap()
    } else {
        app.oneshot(
            Request::builder()
                .method(axum::http::Method::POST)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .uri("/login")
                .body(Body::from(req))
                .unwrap(),
        )
        .await
        .unwrap()
    };

    dbg!(&response);
    dbg!(&response.status());

    let body = to_bytes(response.into_body()).await.unwrap();
    dbg!(&body);

    let body_json = String::from_utf8(body.to_vec()).unwrap();

    let user_json: Value = serde_json::from_str(&body_json).unwrap();
    let token: &str = &user_json["token"].as_str().unwrap();

    let mut token_bearer = "Bearer ".to_owned();
    token_bearer.push_str(token);

    Ok(token_bearer)
}
