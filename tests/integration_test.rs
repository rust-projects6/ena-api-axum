use axum::body::Body;
use axum::http::Request;
use axum::http::StatusCode;

use ena_api_axum::app;
use futures::future;
use hyper::body::to_bytes;
use hyper::header::AUTHORIZATION;
use hyper::header::CONTENT_TYPE;
use serde_json::json;
mod common;

use crate::common::{empty_tables, get_token, Table};

// use serde_json::Value;
use serde_json::Value;
use tower::ServiceExt;

#[tokio::test]
async fn it_hello_world() {
    let app = app(Some(true)).await;

    let response = app
        .oneshot(Request::builder().uri("/").body(Body::empty()).unwrap())
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::OK);

    let body = to_bytes(response.into_body()).await.unwrap();

    assert_eq!(&body[..], b"Hello World!!");
}

#[tokio::test]
#[ignore]
async fn it_create_user() {
    let app = app(Some(true)).await;
    let req = r#"
                 {
                    "username":"hiron",
                    "password":"password"
                 }
                 "#;
    //let req: Value = serde_json::from_str(req).unwrap();

    empty_tables(Table::USERS).await.unwrap();

    let response = app
        .oneshot(
            Request::builder()
                .method(axum::http::Method::POST)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .uri("/signup")
                .body(Body::from(req))
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::CREATED);
}

#[ignore]
#[tokio::test]
async fn it_login_user() {
    let app = app(Some(true)).await;
    let req = r#"
                 {
                    "username":"hiron",
                    "password":"password",
                    "remember": true
                 }
                 "#;
    //let req: Value = serde_json::from_str(req).unwrap();

    let response = app
        .oneshot(
            Request::builder()
                .method(axum::http::Method::POST)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .uri("/login")
                .body(Body::from(req))
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::OK);
}

#[ignore]
#[tokio::test]
async fn it_sign_out_user() {
    let app = app(Some(true)).await;

    let req = r#"
                 {
                    "username":"hiron",
                    "password":"password",
                    "remember": true
                 }
                 "#;
    //let req: Value = serde_json::from_str(req).unwrap();

    let response = app
        .clone()
        .oneshot(
            Request::builder()
                .method(axum::http::Method::POST)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .uri("/login")
                .body(Body::from(req))
                .unwrap(),
        )
        .await
        .unwrap();

    let body = to_bytes(response.into_body()).await.unwrap();
    let body = String::from_utf8(body.to_vec()).unwrap();

    let user_json: Value = serde_json::from_str(&body).unwrap();
    let token: &str = &user_json["token"].as_str().unwrap();

    let mut token_bearer = "Bearer ".to_owned();
    token_bearer.push_str(token);

    let response = app
        .oneshot(
            Request::builder()
                .method(axum::http::Method::GET)
                .header(axum::http::header::AUTHORIZATION, &token_bearer)
                .uri("/signout")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::OK);

    empty_tables(Table::USERS).await.unwrap();
}

#[tokio::test]
async fn it_add_vehicle() {
    let app = app(Some(true)).await;

    let req = r#"{
        "vehicle_no": "D-M-P-234-45",
        "fitness": "2023-07-24",
        "tax": "2023-08-21",
        "insurance": "2023-07-25",
        "route": "2023-08-01",
        "owner": "Ena Transport"
    }"#;

    let token = get_token(app.clone()).await.unwrap();

    let res = app
        .oneshot(
            Request::builder()
                .method(axum::http::Method::POST)
                .header(axum::http::header::AUTHORIZATION, &token)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .uri("/vehicle")
                .body(Body::from(req))
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(res.status(), StatusCode::CREATED);

    empty_tables(Table::VEHICLE).await.unwrap();
}

#[tokio::test]
async fn it_get_all_vehicle() {
    let app = app(Some(true)).await;

    let token = get_token(app.clone()).await.unwrap();

    let res = app
        .oneshot(
            Request::builder()
                .header(CONTENT_TYPE, "application/json")
                .header(AUTHORIZATION, &token)
                .uri("/vehicle")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(res.status(), StatusCode::OK);
}

#[tokio::test]
async fn it_get_fitness() {
    let app = app(Some(true)).await;
    let token = get_token(app.clone()).await.unwrap();

    empty_tables(Table::VEHICLE).await.unwrap();

    let data1 = json!({
        "vehicle_no": "ad-cd-e-12-415",
        "owner": "Hiron",
        "fitness": (chrono::Utc::now()+chrono::Duration::days(7)).date_naive().to_string(),
        "tax": chrono::Utc::now().date_naive(),
        "route": chrono::Utc::now().date_naive(),
        "insurance": chrono::Utc::now().date_naive()
    });

    let data2 = json!({
        "vehicle_no": "ad-cd-e-12-245",
        "owner": "Hiron",
        "fitness": (chrono::Utc::now()+chrono::Duration::days(2)).date_naive().to_string(),
        "tax": chrono::Utc::now().date_naive(),
        "route": chrono::Utc::now().date_naive(),
        "insurance": chrono::Utc::now().date_naive()
    });

    let data3 = json!({
        "vehicle_no": "ad-cd-e-123-45",
        "owner": "Hiron",
        "fitness": (chrono::Utc::now()+chrono::Duration::days(12)).date_naive().to_string(),
        "tax": chrono::Utc::now().date_naive(),
        "route": chrono::Utc::now().date_naive(),
        "insurance": chrono::Utc::now().date_naive()
    });

    let data4 = json!({
        "vehicle_no": "ad-cd-e-24-345",
        "owner": "Hiron",
        "fitness": (chrono::Utc::now()+chrono::Duration::days(18)).date_naive().to_string(),
        "tax": chrono::Utc::now().date_naive(),
        "route": chrono::Utc::now().date_naive(),
        "insurance": chrono::Utc::now().date_naive()
    });

    future::try_join_all(vec![data1, data2, data3, data4].into_iter().map(|item| {
        app.clone().oneshot(
            Request::builder()
                .method(axum::http::Method::POST)
                .header(axum::http::header::AUTHORIZATION, &token)
                .header(axum::http::header::CONTENT_TYPE, "application/json")
                .uri("/vehicle")
                .body(Body::from(serde_json::to_string(&item).unwrap()))
                .unwrap(),
        )
    }))
    .await
    .unwrap();

    let res = app
        .oneshot(
            Request::builder()
                .header(axum::http::header::AUTHORIZATION, &token)
                .uri("/fitness/7")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(res.status(), 200);

    let body = to_bytes(res.into_body()).await.unwrap();
    let body_str = String::from_utf8(body.to_vec()).unwrap();
    let json: Vec<Value> = serde_json::from_str(&body_str).unwrap();

    assert_eq!(json.len(), 2);

    empty_tables(Table::VEHICLE).await.unwrap();
}
